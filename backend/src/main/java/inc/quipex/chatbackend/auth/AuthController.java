package inc.quipex.chatbackend.auth;

import inc.quipex.chatbackend.users.User;
import inc.quipex.chatbackend.users.UserMapper;
import inc.quipex.chatbackend.users.UsersRepository;
import inc.quipex.chatbackend.users.dto.UserResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"${client.origin}"})
@RequestMapping("api/v1")
public class AuthController {
	@Autowired
	private UsersRepository usersRepository;
	
	@PostMapping("login")
	public UserResponseDto login(@RequestBody AuthRequest request) {
		User user = usersRepository.findByLoginAndPassword(request.getLogin(), request.getPassword())
				.orElseThrow(AuthException::new);
		return UserMapper.INSTANCE.userToResponse(user);
	}
}
