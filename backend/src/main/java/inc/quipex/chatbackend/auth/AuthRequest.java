package inc.quipex.chatbackend.auth;

import lombok.Data;

@Data
public class AuthRequest {
	private String login;
	private String password;
}
