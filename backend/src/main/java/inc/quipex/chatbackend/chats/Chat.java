package inc.quipex.chatbackend.chats;

import inc.quipex.chatbackend.BaseEntity;
import inc.quipex.chatbackend.messages.model.Message;
import inc.quipex.chatbackend.users.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "chats")
@Data
@EqualsAndHashCode(callSuper = true)
public class Chat extends BaseEntity {
	
	private String name;

	@ManyToMany
	@JoinTable(name = "chats2users",
	joinColumns = {@JoinColumn(name = "chat_id")},
	inverseJoinColumns = {@JoinColumn(name = "user_id")})
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<User> users;
	
	@OneToMany(mappedBy = "chat")
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<Message> messages;
}
