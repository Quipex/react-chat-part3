package inc.quipex.chatbackend.chats;

import inc.quipex.chatbackend.messages.model.MessageResponseDto;
import inc.quipex.chatbackend.users.UserMapper;
import inc.quipex.chatbackend.users.UsersRepository;
import inc.quipex.chatbackend.users.dto.UserResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/chats")
public class ChatController {
	@Autowired
	private ChatService chatService;
	@Autowired
	private UsersRepository usersRepository;
	@Autowired
	private ChatRepository chatRepository;
	
	@GetMapping("{chatId}/messages")
	public List<MessageResponseDto> getMessages(@PathVariable String chatId, @RequestParam(required = false) String userId) {
		return chatService.getMessages(UUID.fromString(chatId), userId != null ? UUID.fromString(userId) : null);
	}
	
	@GetMapping("{chatId}/users")
	public List<UserResponseDto> getUsers(@PathVariable String chatId) {
		return usersRepository.findAllByChatsContains(chatRepository.getOne(UUID.fromString(chatId))).stream()
				.map(UserMapper.INSTANCE::userToResponse).collect(Collectors.toList());
	}
	
	@GetMapping
	public List<ChatResponseDto> getAll() {
		return chatRepository.findAll().stream().map(ChatMapper.INSTANCE::chatToResponse).collect(Collectors.toList());
	}
}
