package inc.quipex.chatbackend.chats;

import inc.quipex.chatbackend.messages.MessageMapper;
import inc.quipex.chatbackend.users.UserMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {UserMapper.class, MessageMapper.class})
public interface ChatMapper {
	ChatMapper INSTANCE = Mappers.getMapper(ChatMapper.class);
	
	ChatResponseDto chatToResponse(Chat chat);
	
	default long map(List<?> objects) {
		return objects.size();
	}
}
