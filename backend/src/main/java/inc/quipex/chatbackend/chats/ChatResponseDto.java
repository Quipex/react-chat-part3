package inc.quipex.chatbackend.chats;

import lombok.Data;

import java.util.UUID;

@Data
public class ChatResponseDto {
	private UUID id;
	private String createdAt;
	private String updatedAt;
	private String name;
	private long users;
	private long messages;
}
