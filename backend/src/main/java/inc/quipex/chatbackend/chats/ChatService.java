package inc.quipex.chatbackend.chats;

import inc.quipex.chatbackend.messages.MessageMapper;
import inc.quipex.chatbackend.messages.MessagesRepository;
import inc.quipex.chatbackend.messages.model.MessageResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ChatService {
	@Autowired
	private MessagesRepository messagesRepository;
	
	public List<MessageResponseDto> getMessages(UUID chatId, UUID userId) {
		if (userId == null) {
			return messagesRepository.getAllByChat_Id(chatId).stream()
					.map(MessageMapper.INSTANCE::messageToResponse).collect(Collectors.toList());
		} else {
			return messagesRepository.getAllByChatForUser(chatId, userId).stream()
					.map(MessageMapper.INSTANCE::ratedMessageToResponse).collect(Collectors.toList());
		}
	}
}
