package inc.quipex.chatbackend.db;

import inc.quipex.chatbackend.chats.Chat;
import inc.quipex.chatbackend.chats.ChatRepository;
import inc.quipex.chatbackend.messages.MessagesRepository;
import inc.quipex.chatbackend.messages.model.Message;
import inc.quipex.chatbackend.users.Role;
import inc.quipex.chatbackend.users.User;
import inc.quipex.chatbackend.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
@Transactional
public class DatabaseSeeder {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private UsersRepository usersRepository;
	@Autowired
	private ChatRepository chatRepository;
	@Autowired
	private MessagesRepository messagesRepository;
	private final Random random = new Random();
	
	private static String avatar(String text) {
		return "https://api.adorable.io/avatars/285/" + text + ".png";
	}
	
	private static User newUser(String identity, Role role) {
		var user = new User();
		user.setLogin(identity);
		user.setPassword(identity);
		user.setAvatar(avatar(identity));
		user.setRole(role);
		return user;
	}
	
	private static User newUser(String identity) {
		return newUser(identity, Role.USER);
	}
	
	@EventListener
	public void seed(ContextRefreshedEvent event) {
		List<User> u = jdbcTemplate.query("SELECT * FROM users", (resultSet, rowNum) -> null);
		if (u.size() <= 0) {
			seedUsersTable();
			seedChatTable();
			seedMessagesTable();
		}
	}
	
	private void seedUsersTable() {
		usersRepository.saveAll(List.of(newUser("admin", Role.ADMIN), newUser("user"),
				newUser("demo"), newUser("Clark"), newUser("Quipex")));
	}
	
	private void seedChatTable() {
		Chat chat = new Chat();
		var allUsers = usersRepository.findAll();
		chat.setName("My test chat");
		chat.setUsers(allUsers);
		chatRepository.save(chat);
	}
	
	private void seedMessagesTable() {
		String[] texts = new String[] {
				"Великий Гэтсби",
				"@DizelnoeToplivo 🕯️",
				"Кто желает?",
				"за что тут 10800 @hlopochekk",
				"за большой логотип найков",
				"реально вообще найти нормальные по качеству кросы по нормальной цене, а не цене для фанатов лейбла?",
				"а шо тебя интересует? десигн Чи удобство?",
				"удобство + чтобы не разваливались",
				"десинг тоже",
				"я не считаю лейбл адидаса на весь кросовок признаком хорошего десигна, если что",
				"просто мне интересно реально ли сегодня купить хорошие кроссовки не переплачивая за лейбл",
				"доброе утро",
				"А на такие вещи я просто молюсь. Это идеал обуви. Качественно КОООЖАНЫЫЕЕЕ одновременно туфли + ботинки + кроссовки. С супер приятным мехом внутри и достаточно теплой подошвой чтоб фастом не замёрзнуть при морозах. Одним слово - ИМБАНЫ! 10/10 └( ° ͜ʖ͡°)┐.\nС учетом 50% скидки - 2367 грн.",
				"Мой идеал кроссовок. Дырочки для вентиляции + приятный цвет + Memory Foam стелька и всего лишь 1200 грн обошлись по скидке. Ну и должны быть хотя бы на пол размера больше, ибо я никогда в жизни лопатки не юзаю. Если для того чтоб одеть обувь нужна лопатка, то эту обувь носить не стоит. Купил 31 августа в 2169, ношу до сих пор, выглядят вполне сносно.",
				"Охо капец вискарик впрягся\nСразу видно - его тема",
				"Капец, в 2169 году купил, и до сих пор носит",
				"Ну модник, сразу видно рэальна",
				"только вчера пересматривал",
				"рапича инсту",
				"ИМБАНЫ! 10/10 └( ° ͜ʖ͡°)┐",
				"рапич авито в инстагруме завёл",
				"Блин мы снова пропустили стрим ксениипение",
				"Надо уведы поставить на трансляции",
				"как ты могла, алён",
				"Она теперь хочет реже проводить стримы, потому что активность через раз наблюдает......",
				"> как ты могла, алён\n@Quipex как мы могли*\nА то что ты, нас тут много...",
				"Я вчера был на её стриме, но ничего не писал",
				"Она начало стрима с компа начала",
				"Охо",
				"А написано стрим с телефона",
				"Два 2 трансы",
				"А ну ладно, у нее вчера по гороскопу было сказано беречь свое горло, так шо мы бы даже не услышали как она поет или кричит",
				"Неинтересна",
				"Чё?",
				"Это где гороскоп не рекомендует голос напрягать?"
		};
		
		List<Chat> chats = chatRepository.findAll();
		Chat testChat = chats.get(0);
		List<User> users = usersRepository.findAll();
		
		var msgs = Arrays.stream(texts).map(text -> {
			Message msg = new Message();
			msg.setText(text);
			msg.setChat(testChat);
			msg.setAuthor(random(users));
			return msg;
		}).collect(Collectors.toList());
		messagesRepository.saveAll(msgs);
	}
	
	private User random(List<User> users) {
		return users.get(random.nextInt(users.size()));
	}
	
}
