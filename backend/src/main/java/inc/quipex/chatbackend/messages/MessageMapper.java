package inc.quipex.chatbackend.messages;

import inc.quipex.chatbackend.messages.model.Message;
import inc.quipex.chatbackend.messages.model.MessageRequestDto;
import inc.quipex.chatbackend.messages.model.MessageResponseDto;
import inc.quipex.chatbackend.messages.model.MessageWithRating;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MessageMapper {
	MessageMapper INSTANCE = Mappers.getMapper(MessageMapper.class);
	
	@Mapping(target = "chat.id", source = "chatId")
	@Mapping(target = "author.id", source = "authorId")
	Message requestToMessage(MessageRequestDto request);
	
	MessageResponseDto messageToResponse(Message message);
	
	@Mapping(target = "liked", source = "isLiked")
	MessageResponseDto ratedMessageToResponse(MessageWithRating ratedMessage);
}
