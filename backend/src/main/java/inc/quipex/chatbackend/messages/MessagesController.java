package inc.quipex.chatbackend.messages;

import inc.quipex.chatbackend.messages.model.MessageRequestDto;
import inc.quipex.chatbackend.messages.model.MessageResponseDto;
import inc.quipex.chatbackend.messages.model.ToggleLikeRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/messages")
public class MessagesController {
	@Autowired
	private MessagesRepository messagesRepository;
	@Autowired
	private MessagesService messagesService;
	
	@PostMapping("update")
	public MessageResponseDto update(@RequestBody MessageRequestDto message) {
		return MessageMapper.INSTANCE.messageToResponse(messagesService.update(message.getText(), message.getId()));
	}
	
	@PostMapping("create")
	@Transactional
	public MessageResponseDto create(@RequestBody MessageRequestDto request) {
		return MessageMapper.INSTANCE.messageToResponse(
				messagesService.save(request.getText(), request.getAuthorId(), request.getChatId()));
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable String id) {
		messagesService.deleteById(UUID.fromString(id));
	}
	
	@PutMapping("like")
	@Transactional
	public boolean toggleLike(@RequestBody ToggleLikeRequestDto request) {
		return messagesService.toggleLike(UUID.fromString(request.getMessageId()), UUID.fromString(request.getUserId()));
	}
}
