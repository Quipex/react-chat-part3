package inc.quipex.chatbackend.messages;

import inc.quipex.chatbackend.messages.model.Message;
import inc.quipex.chatbackend.messages.model.MessageWithRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MessagesRepository extends JpaRepository<Message, UUID> {
	
	List<Message> getAllByChat_Id(UUID chatId);
	
	@Query("update Message set text=:text where id=:messageId")
	@Modifying
	void update(String text, UUID messageId);
	
	@SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
	@Query("select m.id as id, m.createdAt as createdAt, m.updatedAt as updatedAt, m.text as text, m.author as author, " +
			"(m in (select lm from User u join u.likedMessages lm where u.id=:userId)) as isLiked " +
			"from Message m where m.chat.id=:chatId")
	List<MessageWithRating> getAllByChatForUser(UUID chatId, UUID userId);
}
