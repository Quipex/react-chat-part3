package inc.quipex.chatbackend.messages;

import inc.quipex.chatbackend.chats.ChatRepository;
import inc.quipex.chatbackend.messages.model.Message;
import inc.quipex.chatbackend.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MessagesService {
	@Autowired
	private MessagesRepository messagesRepository;
	@Autowired
	private UsersRepository usersRepository;
	@Autowired
	private ChatRepository chatRepository;
	
	@Transactional
	public boolean toggleLike(UUID messageId, UUID userId) {
		Message likedMessage = messagesRepository.getOne(messageId);
		var foundUser = usersRepository.findByIdAndLikedMessagesContains(userId, likedMessage);
		if (foundUser.isPresent()) {
			var savedUser = foundUser.get();
			List<Message> otherMessages = savedUser.getLikedMessages().stream()
					.filter(m -> !m.getId().equals(messageId)).collect(Collectors.toList());
			savedUser.setLikedMessages(otherMessages);
			return false;
		} else {
			var userLiked = usersRepository.findById(userId).orElseThrow();
			List<Message> likedMessages = userLiked.getLikedMessages();
			likedMessages.add(likedMessage);
			userLiked.setLikedMessages(likedMessages);
			return true;
		}
	}
	
	public Message save(String text, UUID authorId, UUID chatId) {
		var user = usersRepository.getOne(authorId);
		var chat = chatRepository.getOne(chatId);
		var newMessage = new Message();
		newMessage.setText(text);
		newMessage.setAuthor(user);
		newMessage.setChat(chat);
		return messagesRepository.saveAndFlush(newMessage); // need to flush because otherwise timestamps are not set
	}
	
	@Transactional
	public Message update(String text, UUID id) {
		Message savedMsg = messagesRepository.findById(id).orElseThrow();
		savedMsg.setText(text);
		return savedMsg;
	}
	
	@Transactional
	public void deleteById(UUID id) {
		Message message = messagesRepository.findById(id).orElseThrow();
		message.setDeleted(true);
	}
}
