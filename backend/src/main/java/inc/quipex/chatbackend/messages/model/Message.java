package inc.quipex.chatbackend.messages.model;

import inc.quipex.chatbackend.BaseEntity;
import inc.quipex.chatbackend.chats.Chat;
import inc.quipex.chatbackend.users.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Table(name = "messages")
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_deleted=false")
public class Message extends BaseEntity {
	@Column(nullable = false, length = 4096)
	private String text;
	
	@ManyToOne(cascade = {CascadeType.REFRESH})
	private User author;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	private Chat chat;
}
