package inc.quipex.chatbackend.messages.model;

import lombok.Data;

import java.util.UUID;

@Data
public class MessageRequestDto {
	private UUID id;
	private String text;
	private UUID authorId;
	private UUID chatId;
}
