package inc.quipex.chatbackend.messages.model;

import inc.quipex.chatbackend.users.dto.UserResponseDto;
import lombok.Data;

import java.util.UUID;

@Data
public class MessageResponseDto {
	private UUID id;
	private String createdAt;
	private String updatedAt;
	private String text;
	private UserResponseDto author;
	private boolean isLiked;
}
