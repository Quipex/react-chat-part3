package inc.quipex.chatbackend.messages.model;

import inc.quipex.chatbackend.users.User;

import java.time.LocalDateTime;
import java.util.UUID;

public interface MessageWithRating {
	UUID getId();
	LocalDateTime getCreatedAt();
	LocalDateTime getUpdatedAt();
	String getText();
	User getAuthor();
	boolean getIsLiked();
}
