package inc.quipex.chatbackend.messages.model;

import lombok.Data;

@Data
public class ToggleLikeRequestDto {
	private String userId;
	private String messageId;
}
