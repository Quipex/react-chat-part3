package inc.quipex.chatbackend.users;

public enum Role {
	USER,
	ADMIN
}
