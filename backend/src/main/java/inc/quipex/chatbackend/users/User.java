package inc.quipex.chatbackend.users;

import inc.quipex.chatbackend.BaseEntity;
import inc.quipex.chatbackend.chats.Chat;
import inc.quipex.chatbackend.messages.model.Message;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;

@Table(name = "users")
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_deleted=false")
public class User extends BaseEntity {
	
	private String login;
	
	private String password;
	
	private String avatar;
	
	@Enumerated(EnumType.STRING)
	private Role role;
	
	@ManyToMany
	@JoinTable(name = "users2liked_messages",
			joinColumns = {@JoinColumn(name = "user_id")},
			inverseJoinColumns = {@JoinColumn(name = "message_id")})
	private List<Message> likedMessages;
	
	@ManyToMany(mappedBy = "users")
	private List<Chat> chats;
}


