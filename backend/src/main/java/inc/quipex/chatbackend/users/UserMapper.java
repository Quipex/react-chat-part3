package inc.quipex.chatbackend.users;

import inc.quipex.chatbackend.users.dto.UserRequestDto;
import inc.quipex.chatbackend.users.dto.UserResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
	
	UserResponseDto userToResponse(User user);
	
	User requestToUser(UserRequestDto user);
	
}
