package inc.quipex.chatbackend.users;

import inc.quipex.chatbackend.users.dto.UserRequestDto;
import inc.quipex.chatbackend.users.dto.UserResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/users")
public class UsersController {
	@Autowired
	private UsersRepository repository;
	@Autowired
	private UsersService service;
	
	@GetMapping
	public List<UserResponseDto> getAll() {
		return repository.findAll().stream().map(UserMapper.INSTANCE::userToResponse)
				.collect(Collectors.toList());
	}
	
	@GetMapping("{id}")
	public UserResponseDto get(@PathVariable("id") String userId) {
		return repository.findById(UUID.fromString(userId)).map(UserMapper.INSTANCE::userToResponse)
				.orElseThrow();
	}
	
	@PostMapping("create")
	@ResponseStatus(HttpStatus.CREATED)
	public UserResponseDto create(@RequestBody UserRequestDto request) {
		return UserMapper.INSTANCE.userToResponse(repository.save(UserMapper.INSTANCE.requestToUser(request)));
	}
	
	@DeleteMapping("{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") String userId) {
		service.deleteById(UUID.fromString(userId));
	}
	
	@PostMapping("update")
	public UserResponseDto update(@RequestBody UserRequestDto request) {
		var updatedUser = service.update(request.getId(), request.getLogin(), request.getPassword(), request.getAvatar(), request.getRole());
		return UserMapper.INSTANCE.userToResponse(updatedUser);
	}
}
