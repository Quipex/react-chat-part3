package inc.quipex.chatbackend.users;

import inc.quipex.chatbackend.chats.Chat;
import inc.quipex.chatbackend.messages.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UsersRepository extends JpaRepository<User, UUID> {
	
	List<User> findAllByChatsContains(Chat chat);
	
	Optional<User> findByIdAndLikedMessagesContains(UUID userId, Message message);
	
	Optional<User> findByLoginAndPassword(String login, String password);
}
