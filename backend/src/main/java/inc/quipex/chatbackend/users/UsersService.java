package inc.quipex.chatbackend.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.UUID;

@Service
public class UsersService {
	@Autowired
	private UsersRepository usersRepository;
	
	@Transactional
	public User update(UUID id, String login, String password, String avatar, String role) {
		User user = usersRepository.findById(id).orElseThrow();
		if (login != null) user.setLogin(login);
		if (password != null) user.setPassword(password);
		if (avatar != null) user.setAvatar(avatar);
		if (role != null) user.setRole(Role.valueOf(role));
		return user;
	}
	
	@Transactional
	public void deleteById(UUID id) {
		User user = usersRepository.findById(id).orElseThrow();
		user.setDeleted(true);
	}
}
