package inc.quipex.chatbackend.users.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class UserRequestDto {
	private UUID id;
	private String login;
	private String password;
	private String avatar;
	private String role;
}
