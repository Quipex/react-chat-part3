package inc.quipex.chatbackend.users.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class UserResponseDto {
	private UUID id;
	private String createdAt;
	private String updatedAt;
	private String login;
	private String password;
	private String avatar;
	private String role;
}
