import React from 'react';
import './App.css';
import './styles/reset.scss';
import 'semantic-ui-css/semantic.min.css';
import './styles/common.scss';
import {Route, Switch,} from "react-router-dom";
import PrivateRoute from './components/routes/private/PrivateRoute';
import PublicRoute from './components/routes/public/PublicRoute';
import NotFoundPage from './components/views/not-found/NotFoundPage';
import ChatPage from './components/views/chat/ChatPage';
import UsersListPage from './components/views/user-list/UserListPage';
import UsersEditorPage from './components/views/user-edit/UserEditPage';
import EditMessagePage from './components/views/edit-message/EditMessagePage';
import LoginPage from './components/views/login/LoginPage';
// @ts-ignore
import {NotificationContainer} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

export default function App() {
    return (
        <>
            <Switch>
                <PublicRoute exact path="/login" component={LoginPage}/>
                <PrivateRoute exact path="/" component={ChatPage}/>
                <PrivateRoute exact path="/admin/users" component={UsersListPage}/>
                <PrivateRoute exact path="/admin/users/edit*" component={UsersEditorPage}/>
                <PrivateRoute path="/admin/message/:messageId" component={EditMessagePage}/>
                <Route path="*" exact component={NotFoundPage}/>
            </Switch>
            <NotificationContainer/>
        </>
    )
};

