import React from 'react';
import styles from './styles.module.scss';

export default function Footer() {
    return (
        <div className={styles.footer}>Copyright 2020. Made by Quipex with bare hands.</div>
    )
}
