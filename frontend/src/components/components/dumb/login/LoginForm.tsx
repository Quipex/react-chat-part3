import React, {useState} from 'react';
import {Button, Form, Segment} from 'semantic-ui-react';
import validator from 'validator';

export interface LoginFormProps {
    login: (username: string, password: string) => void
}

export default function LoginForm({login}: LoginFormProps) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [isUsernameValid, setIsUsernameValid] = useState(true);
    const [isPasswordValid, setIsPasswordValid] = useState(true);

    const usernameChanged = (data: string) => {
        setUsername(data);
        setIsUsernameValid(true);
    };

    const passwordChanged = (data: string) => {
        setPassword(data);
        setIsPasswordValid(true);
    };

    const handleLoginClick = async () => {
        const isValid = isUsernameValid && isPasswordValid;
        if (!isValid || isLoading) {
            return;
        }
        setIsLoading(true);
        login(username, password);
        setIsLoading(false);
    };

    return (
        <Form name="loginForm" size="large" onSubmit={handleLoginClick}>
            <Segment>
                <Form.Input
                    fluid
                    icon="male"
                    iconPosition="left"
                    placeholder="Username"
                    type="login"
                    error={!isUsernameValid}
                    onChange={ev => usernameChanged(ev.target.value)}
                    onBlur={() => setIsUsernameValid(validator.isAlphanumeric(username))}
                />
                <Form.Input
                    fluid
                    icon="lock"
                    iconPosition="left"
                    placeholder="Password"
                    type="password"
                    error={!isPasswordValid}
                    onChange={ev => passwordChanged(ev.target.value)}
                    onBlur={() => setIsPasswordValid(Boolean(password))}
                />
                <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
                    Login
                </Button>
            </Segment>
        </Form>
    )
}
