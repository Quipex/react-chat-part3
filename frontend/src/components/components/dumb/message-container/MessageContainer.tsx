import React from 'react';
import styles from './styles.module.scss';
import moment from 'moment';
import {Button, Segment} from 'semantic-ui-react';
import {Message} from '../../../../state/model-types';

export interface MessageComponent {
    message: Message,
    className: string,
    setEditedMsg: (msg: Message) => void,
    deleteMsg: (messageId: string) => void,
    likeMsg: (messageId: string) => void,
    setConfiguredMsg: (msg: Message) => void,
    userId: string,
    isEdited: boolean,
    displayAvatar: boolean,
    isDeleting: boolean,
    isEditing: boolean,
    isLiking: boolean
}

export function MessageContainer(
    {
        message,
        className = '',
        setEditedMsg,
        deleteMsg,
        likeMsg,
        setConfiguredMsg,
        userId,
        isEdited = false,
        displayAvatar = true,
        isEditing = false,
        isDeleting = false,
        isLiking = false
    }: MessageComponent
) {
    const edited = message.updatedAt !== undefined && message.updatedAt !== '';
    const timestamp_label = edited ? 'edited ' + moment(message.updatedAt).fromNow() :
        moment(message.createdAt).fromNow();
    const timestamp = edited ? 'edited ' + moment(message.updatedAt).format('LLL') :
        moment(message.createdAt).format('LLL');

    return (
        <Segment className={`${styles.message} ${className}`}>
            {displayAvatar && (
                <div className={styles.avatar}>
                    {
                        message.author.avatar !== '' && message.author.avatar !== undefined ?
                            (<img src={message.author.avatar} alt={message.author.login} className={styles.avatar}/>) :
                            (<div className={`${styles.no_avatar} ${styles.avatar}`}>
                                <span className={styles.initials}>{message.author.login?.charAt(0)}</span>
                            </div>)
                    }
                </div>
            )}
            <span className={displayAvatar ? styles.username : styles.username__no_avatar}>{message.author.login}</span>
            <span className={styles.timestamp} aria-label={timestamp} title={timestamp}>{timestamp_label}</span>
            <div className={displayAvatar ? styles.text : styles.text__no_avatar}>{message.text}</div>
            <Button className={styles.cogButton} icon='cog' onClick={() => setConfiguredMsg(message)} />
            <div className={styles.modify_buttons}>
                {userId !== message.author.id && (
                    <Button icon={message.liked ? 'heart' : 'heart outline'} color={message.liked ? 'red' : 'grey'}
                            onClick={() => likeMsg(message.id as string)} loading={isLiking} />
                )}
                {userId === message.author.id && (
                    <>
                        <Button icon='edit' color='grey'
                                onClick={() => setEditedMsg(message)}
                                disabled={isEdited}
                                loading={isEditing}
                        />
                        <Button icon='trash alternate' color='grey'
                                onClick={() => deleteMsg(message.id as string)}
                                disabled={isEdited}
                                loading={isDeleting}
                        />
                    </>
                )}
            </div>
        </Segment>
    )
}
