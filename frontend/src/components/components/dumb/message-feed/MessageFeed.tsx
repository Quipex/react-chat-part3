import React from 'react';
import './styles.module.scss'
import styles from './styles.module.scss'
import moment from 'moment';
import {TitledMessageBlock} from '../../smart/titled-message-block/TitledMessageBlock';
import {Message, User} from '../../../../state/model-types';

export interface MessageFeedComponent {
    messages: Array<Message>,
    sender: User,
    setEditedMessage: (msg: Message) => void,
    editedMessage?: Message,
    setConfiguredMessage: (msg: Message) => void,
}

export function MessageFeed(
    {
        messages,
        sender,
        setEditedMessage,
        editedMessage,
        setConfiguredMessage,
    }: MessageFeedComponent
) {
    const dateToMessages = new Map<string, Array<Message>>();
    messages.sort((m1, m2) => moment(m1.createdAt).diff(m2.createdAt))
        .forEach(msg => {
            const day = moment(msg.createdAt).startOf('day').format('DD.MM.yyyy');
            let array = dateToMessages.get(day);
            if (array === undefined) {
                array = []
            }
            array.push(msg);
            dateToMessages.set(day, array);
        });
    const dates = [];
    // @ts-ignore
    for (const item of dateToMessages.keys()) {
        dates.push(item);
    }

    return (
        <div className={styles.messageFeed}>
            {dates.map(date => (
                <TitledMessageBlock title={date}
                                    messages={dateToMessages.get(date) as Message[]}
                                    sender={sender}
                                    setEditedMessage={setEditedMessage}
                                    setConfiguredMessage={setConfiguredMessage}
                                    editedMessage={editedMessage} key={date}
                />
            ))}
        </div>
    )
}
