import React from 'react';
import {Button, Table, TableBody, TableCell, TableHeader, TableHeaderCell, TableRow} from 'semantic-ui-react';
import {User} from '../../../../state/model-types';

interface UserTableProps {
    users: Array<User>,
    editUser: (id: string) => void,
    deleteUser: (id: string) => void,
    isDeleting: boolean,
    idDeleting: string | undefined
}

export function UserTable({users, editUser, deleteUser, isDeleting, idDeleting}: UserTableProps) {
    return (
        <Table celled striped compact>
            <TableHeader>
                <TableHeaderCell>ID</TableHeaderCell>
                <TableHeaderCell>Login</TableHeaderCell>
                <TableHeaderCell>Role</TableHeaderCell>
                <TableHeaderCell colSpan={2}>Actions</TableHeaderCell>
            </TableHeader>
            <TableBody>
                {users.map(user => (
                    <TableRow key={user.id}>
                        <TableCell>{user.id}</TableCell>
                        <TableCell>{user.login}</TableCell>
                        <TableCell>{user.role}</TableCell>
                        <TableCell textAlign='right'>
                            <Button onClick={() => editUser(user.id)} content='Edit' color='yellow' icon='edit'/>
                        </TableCell>
                        <TableCell textAlign='right'>
                            <Button onClick={() => deleteUser(user.id)} content='Delete' color='red' icon='trash'
                                    loading={isDeleting && user.id === idDeleting}
                            />
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    )
}
