import {User} from '../../../../state/model-types';
import React from 'react';
import {Button, TableCell, TableRow} from 'semantic-ui-react';

export interface UserRowProps {
    user: User,
    editUser: (userId: string) => void,
    deleteUser: (userId: string) => void
}

export function UserRow(
    {
        user,
        editUser,
        deleteUser
    }: UserRowProps) {
    return (
        <TableRow>
            <TableCell>{user.id}</TableCell>
            <TableCell>{user.login}</TableCell>
            <TableCell>{user.password}</TableCell>
            <TableCell>{user.avatar}</TableCell>
            <TableCell>{user.role}</TableCell>
            <TableCell><Button onClick={() => editUser(user.id)} content='Edit'/></TableCell>
            <TableCell><Button onClick={() => deleteUser(user.id)} content='Delete'/></TableCell>
        </TableRow>
    )
}
