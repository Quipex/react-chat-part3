import React, {useEffect, useRef} from 'react';
import {connect, useDispatch} from 'react-redux';
import {bindActionCreators} from 'redux'
import Header from '../header/Header';
import {MessageFeed} from '../../dumb/message-feed/MessageFeed';
import {MessageInput} from '../../dumb/message-input/MessageInput';
import moment from 'moment';
import styles from './styles.module.scss';
import Ref from 'semantic-ui-react/dist/commonjs/addons/Ref';
import {setConfiguredMessage, setEditedMessage} from '../../../../state/chat/actions';
import AppState from '../../../../state/state-type';
import Spinner from '../../dumb/spinner/Spinner';
import Footer from '../../dumb/footer/Footer';
import {editMessageRequestAction, sendMessageRequestAction} from '../../../../state/chat/sagas';
import {Message, User} from '../../../../state/model-types';

export interface ChatComponentProps {
    chatId?: string,
    chatName?: string,
    messages?: Array<Message>,
    users?: Array<User>,
    editedMessage?: Message,
    configuredMessage?: Message,
    sender: User,
    setEditedMessage: (msg: Message | undefined) => void,
    setConfiguredMessage: (msg: Message | undefined) => void,
    isLoading: boolean,
}

function ChatComponent(
    {
        chatId,
        messages,
        editedMessage,
        sender,
        setEditedMessage: setEditedMsg,
        setConfiguredMessage: setConfiguredMsg,
        isLoading
    }: ChatComponentProps) {
    const bottomRef = useRef(null);
    const stickyHeaderRef = useRef(null);
    const dispatch = useDispatch();

    function sortMessagesFromLatest(messages: Array<Message>) {
        return messages.sort((m1, m2) =>
            moment(m2.createdAt).diff(m1.createdAt));
    }

    function setLastMessageEdited() {
        if (messages && sender && messages.length > 0) {
            const filtered = messages.filter(m => m.author.id === sender.id);
            if (filtered.length > 0) {
                const messages1 = sortMessagesFromLatest(filtered);
                setEditedMsg(messages1[0])
            }
        }
    }

    function cancelMessageEditing() {
        setEditedMsg(undefined);
    }

    const messagesLength = messages?.length;
    useEffect(() => {
        // @ts-ignore
        bottomRef.current.scrollIntoView({behavior: 'auto'});
    }, [messagesLength]);

    function handleSendMessage(message: Message) {
        if (message.id === undefined || message.id === '') {
            dispatch(sendMessageRequestAction(sender, message.text, chatId as string))
        } else {
            dispatch(editMessageRequestAction(message.id, message.text))
        }
    }

    return (
        <div className={styles.chat}>
            <div className={styles.chat_area}>
                <img src="https://i.imgur.com/PLFeGFW.png" alt="Logo" className={styles.logo}/>
                <Ref innerRef={stickyHeaderRef}>
                    {/* <Ref> needs to have only one child node, that's why <div> is wrapping*/}
                    <div>
                        <Header sticky_ref={stickyHeaderRef}/>
                        {sender && messages ? (
                            <MessageFeed sender={sender} messages={messages}
                                         setEditedMessage={setEditedMsg} editedMessage={editedMessage}
                                         setConfiguredMessage={setConfiguredMsg}
                            />
                        ) : <Spinner/>}

                    </div>
                </Ref>
                <div ref={bottomRef as any}/>
            </div>
            {sender && (
                <MessageInput className={styles.input_area} sender={sender} sendMessage={handleSendMessage}
                              messageId={editedMessage?.id} messageText={editedMessage?.text}
                              cancelUpdatingMessage={() => cancelMessageEditing()}
                              setLastMessageEdited={() => setLastMessageEdited()}/>
            )}
            <footer>
                <Footer/>
            </footer>
            {isLoading && (<Spinner />)}
        </div>
    )
}

// noinspection JSUnusedGlobalSymbols
const actions = {
    setEditedMessage,
    setConfiguredMessage,
};

const mapDispatchToProps = (dispatch: any) => bindActionCreators(actions, dispatch);

const mapStateToProps = (state: AppState) => ({
    sender: state.profile.user,
    chatId: state.chat.chatId,
    chatName: state.chat.chatName,
    messages: state.chat.messages,
    users: state.chat.users,
    editedMessage: state.chat.editedMessage,
    isLoading: state.async_fetch_chat.loading
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
// @ts-ignore
)(ChatComponent);
