import React, {RefObject} from 'react';
import {Dropdown, DropdownHeader, DropdownItem, DropdownMenu, Icon, Menu, Sticky} from 'semantic-ui-react';
import styles from './styles.module.scss';
import {Message, Role, User} from '../../../../state/model-types';
import AppState from '../../../../state/state-type';
import {useHistory} from 'react-router-dom';
import {connect, useDispatch} from 'react-redux';
import {setCurrentUser} from '../../../../state/profile/actions';
import moment from 'moment';


export interface HeaderComponentProps {
    chat_name: string,
    members: number,
    messages: Array<Message>,
    sticky_ref: RefObject<unknown>,
    current_user: User
}

function Header(
    {
        chat_name,
        members,
        messages,
        sticky_ref,
        current_user
    }: HeaderComponentProps) {
    const dispatch = useDispatch();
    const history = useHistory();

    function logOut() {
        history.push('/login')
        dispatch(setCurrentUser(undefined));
    }

    function sortMessagesFromLatest(messages: Array<Message>) {
        return messages.sort((m1, m2) =>
            moment(m2.createdAt).diff(m1.createdAt));
    }

    function last_message_timestamp(messages: Array<Message>) {
        return messages.length === 0 ? 'never' : moment((sortMessagesFromLatest(messages))[0].createdAt).fromNow();
    }

    function goToUsers() {
        history.push('admin/users')
    }

    return (
        <Sticky context={sticky_ref} className={styles.menu}>
            <Menu className="component-header">
                <Menu.Item className={styles.chatName} title={chat_name}>
                    {chat_name}
                </Menu.Item>
                <Menu.Item><Icon name='group'/>{members}</Menu.Item>
                <Menu.Item><Icon name='mail'/>{messages.length}</Menu.Item>
                <Menu.Item position={'right'}>Last message {last_message_timestamp(messages)}</Menu.Item>
                <Menu.Item>
                    <Dropdown text={current_user.login} direction='left'>
                        <DropdownMenu>
                            <DropdownHeader>Profile</DropdownHeader>
                            {current_user.role === Role.ADMIN && (
                                <DropdownItem onClick={() => goToUsers()}>Users</DropdownItem>
                            )}
                            <DropdownItem icon='log out' onClick={logOut} content='Log out'/>
                        </DropdownMenu>
                    </Dropdown>
                </Menu.Item>
            </Menu>
        </Sticky>
    )
}

const mapStateToProps = (state: AppState) => ({
    current_user: state.profile.user,
    chat_name: state.chat.chatName,
    members: state.chat.users.length,
    messages: state.chat.messages,
})

// @ts-ignore
export default connect(mapStateToProps)(Header)
