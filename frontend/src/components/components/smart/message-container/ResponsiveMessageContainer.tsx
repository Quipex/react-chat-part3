import React from 'react';
import {MessageContainer} from '../../dumb/message-container/MessageContainer';
import AppState from '../../../../state/state-type';
import {connect, useDispatch} from 'react-redux';
import {deleteMessageRequestAction, likeMessageRequestAction} from '../../../../state/chat/sagas';
import {setConfiguredMessage, setEditedMessage} from '../../../../state/chat/actions';
import {Message} from '../../../../state/model-types';
import {useHistory} from 'react-router-dom';

export interface ResponsiveMessageContainerProps {
    message: Message,
    currentEditedMsg: Message,
    displayAvatar: boolean,
    isDeleting: boolean,
    currentDeletedId: string,
    userId: string,
    isEditing: boolean,
    isLiking: boolean,
    currentEditedId: string,
    currentLikedId: string,
    className: string
}

function ResponsiveMessageContainer(
    {
        message,
        currentEditedMsg,
        displayAvatar,
        isDeleting,
        currentDeletedId,
        userId,
        isEditing,
        isLiking,
        currentEditedId,
        currentLikedId,
        className
    }: ResponsiveMessageContainerProps) {
    const dispatch = useDispatch();
    const history = useHistory();

    function setEditedMsg(message: Message) {
        dispatch(setEditedMessage(message));
    }

    function deleteMsg(messageId: string) {
        dispatch(deleteMessageRequestAction(messageId));
    }

    function likeMsg(messageId: string) {
        dispatch(likeMessageRequestAction(messageId, userId));
    }

    function setConfiguredMsg(message: Message) {
        dispatch(setConfiguredMessage(message));
        history.push('/admin/message/' + message.id)
    }

    return (
        <MessageContainer message={message}
                          userId={userId}
                          isEdited={message.id === currentEditedMsg?.id}
                          displayAvatar={displayAvatar}
                          isDeleting={isDeleting && message.id === currentDeletedId}
                          isEditing={isEditing && message.id === currentEditedId}
                          className={className}
                          isLiking={isLiking && message.id === currentLikedId}
                          setEditedMsg={setEditedMsg}
                          deleteMsg={deleteMsg}
                          likeMsg={likeMsg}
                          setConfiguredMsg={setConfiguredMsg}
        />
    )
}

const mapStateToProps = (store: AppState) => ({
    currentDeletedId: store.async_delete_message.payload?.messageId,
    currentEditedId: store.async_edit_message.payload?.id,
    currentLikedId: store.async_like_message.payload?.messageId,
    isDeleting: store.async_delete_message.loading,
    isEditing: store.async_edit_message.loading,
    isLiking: store.async_like_message.loading,
    currentEditedMsg: store.chat.editedMessage
});

// @ts-ignore
export default connect(mapStateToProps)(ResponsiveMessageContainer);
