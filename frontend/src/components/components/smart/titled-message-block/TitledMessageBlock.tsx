import {Label, Sticky} from 'semantic-ui-react';
import React, {useRef} from 'react';
import styles from './styles.module.scss';
import Ref from 'semantic-ui-react/dist/commonjs/addons/Ref';
import ResponsiveMessageContainer from '../message-container/ResponsiveMessageContainer';
import {Message, User} from '../../../../state/model-types';

export interface TitledMessageBlockProps {
    title: string,
    messages: Array<Message>,
    sender: User,
    setEditedMessage: (msg: Message) => void,
    editedMessage?: Message,
    setConfiguredMessage: (msg: Message) => void,
}

export function TitledMessageBlock(
    {
        title,
        messages,
        sender,
        editedMessage
    }: TitledMessageBlockProps) {
    const sticky_ref = useRef(null);

    return (
        <Ref innerRef={sticky_ref}>
            <div className={styles.sticky_container}>
                <Sticky context={sticky_ref} offset={45}>
                    <div className={styles.date_label}>
                        <Label>{title}</Label>
                    </div>
                </Sticky>
                <div className={styles.messageFeed}>
                    {messages.map(message => (
                        <ResponsiveMessageContainer
                            message={message} key={message.id}
                            className={`${sender.id === message.author.id ? styles.your_message : styles.their_message}
                                     ${message === editedMessage ? styles.edited_message : ''}`}
                            userId={sender.id}
                            isEdited={message === editedMessage}
                            displayAvatar={sender.id !== message.author.id}
                        />))}
                </div>
            </div>
        </Ref>
    )
}
