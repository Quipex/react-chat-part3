import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import AppState from '../../../state/state-type';


const PrivateRoute = ({ component: Component, isAuthorized, role, ...rest }: any) => (
  <Route
    {...rest}
    render={props => (isAuthorized
      ? <Component {...props} />
      : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />)}
  />
);

const mapStateToProps = (rootState: AppState) => ({
  isAuthorized: rootState.profile.user !== undefined,
  role: rootState.profile.user?.role
});

export default connect(mapStateToProps)(PrivateRoute);
