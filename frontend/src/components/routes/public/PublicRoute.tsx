import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import AppState from '../../../state/state-type';

const PublicRoute = ({ component: Component, isAuthorized, ...rest }: any) => (
  <Route
    {...rest}
    render={props => (isAuthorized
      ? <Redirect to={{ pathname: '/', state: { from: props.location } }} />
      : <Component {...props} />)}
  />
);

const mapStateToProps = (rootState: AppState) => ({
  isAuthorized: rootState.profile.user !== undefined
});

export default connect(mapStateToProps)(PublicRoute);
