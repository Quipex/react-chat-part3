import React, {useEffect} from 'react';
import ChatComponent from '../../components/smart/chat/ChatComponent';
import {connect, useDispatch} from 'react-redux';
import {fetchChatRequestAction} from '../../../state/chat/sagas';
import AppState from '../../../state/state-type';

function ChatPage({userId}: any) {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchChatRequestAction(userId) as any)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    return (
        <ChatComponent/>
    )
}

const mapStateToProps = (store: AppState) => ({
    userId: store.profile.user?.id
});

export default connect(mapStateToProps)(ChatPage);


