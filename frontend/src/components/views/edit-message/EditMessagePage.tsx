import styles from './styles.module.scss';
import React, {useEffect} from 'react';
import {connect, useDispatch} from 'react-redux';
import AppState from '../../../state/state-type';
import {Message, User} from '../../../state/model-types';
import {MessageInput} from '../../components/dumb/message-input/MessageInput';
import {editMessageRequestAction} from '../../../state/chat/sagas';
import {useHistory} from 'react-router-dom'

interface EditMessagePageProps {
    message?: Message,
    sender: User
}

function EditMessagePage(
    {
        message,
        sender
    }: EditMessagePageProps) {
    const dispatch = useDispatch();
    const history = useHistory();

    function handleConfigureMessage(_message: Message) {
        dispatch(editMessageRequestAction(_message.id as string, _message.text));
    }

    useEffect(() => {
        if (message === undefined) {
            history.push('/');
        }
    }, [history, message]);

    return (
        <div className={styles.container}>
            <MessageInput sendMessage={handleConfigureMessage} sender={sender}
                          messageText={message?.text} messageId={message?.id}
                          setLastMessageEdited={() => {
                          }} cancelUpdatingMessage={() => {
            }}/>
        </div>
    )
}

const mapStateToProps = (state: AppState) => ({
    message: state.chat.configuredMessage,
    sender: state.profile.user
})


// @ts-ignore
export default connect(mapStateToProps)(EditMessagePage)
