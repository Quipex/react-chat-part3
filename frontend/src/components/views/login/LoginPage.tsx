import React from 'react';
import {Grid, Header, Message} from "semantic-ui-react";
import LoginForm from '../../components/dumb/login/LoginForm';
import {connect, useDispatch} from 'react-redux';
import {loginRequestAction} from '../../../state/profile/sagas';
import AppState from '../../../state/state-type';
import Spinner from '../../components/dumb/spinner/Spinner';

export function LoginPage({isLoading, error}: any) {
    const dispatch = useDispatch();

    function login(username: string, password: string) {
        dispatch(loginRequestAction(username, password))
    }

    return (
        <div style={{display: 'flex', height: '100%'}}>
            <Grid textAlign="center" verticalAlign="middle" className="fill">
                <Grid.Column style={{maxWidth: 450}}>
                    <Header as="h2" color="teal" textAlign="center">
                        Login to your account
                    </Header>
                    {error && <Message error>{error.message}</Message>}
                    <LoginForm login={login}/>
                </Grid.Column>
            </Grid>
            {isLoading && <Spinner/>}
        </div>
    )
}

const mapStateToProps = (state: AppState) => {
    return ({
        isLoading: state.async_login.loading,
        error: state.async_login.error
    });
}

export default connect(
    mapStateToProps
)(LoginPage);
