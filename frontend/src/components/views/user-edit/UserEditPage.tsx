import React, {useEffect, useState} from 'react';
import {Button, Form, FormButton, FormDropdown, FormInput} from 'semantic-ui-react';
import {Role, User} from '../../../state/model-types';
import AppState from '../../../state/state-type';
import {connect, useDispatch} from 'react-redux';
import {useHistory, useLocation} from 'react-router-dom';
import {createUserRequestAction, editUserRequestAction, fetchUserAndSetEdited} from '../../../state/user-list/sagas';
import {setEditedUser} from '../../../state/user-list/actions';
import styles from './styles.module.scss';

interface UserEditPageProps {
    user: User | undefined,
    isEditing: boolean,
    isFetching: boolean
}

function UsersEditorPage({user, isEditing, isFetching}: UserEditPageProps) {
    const location = useLocation();
    const history = useHistory();
    const dispatch = useDispatch();
    const [userId, setUserId] = useState('');
    const [userLogin, setUserLogin] = useState('')
    const [userPassword, setUserPassword] = useState('')
    const [userAvatar, setUserAvatar] = useState('')
    const [userRole, setUserRole] = useState(Role.USER)

    function extractUserId(url: string) {
        const indexOfLastSlash = url.lastIndexOf('/');
        if (indexOfLastSlash === url.length - 1) {
            return '';
        } else {
            return url.substring(indexOfLastSlash + 1);
        }
    }

    function handleSave() {
        if (userId !== undefined && userId !== '') {
            dispatch(editUserRequestAction({
                id: userId, role: userRole, password: userPassword,
                avatar: userAvatar, login: userLogin
            }))
        } else {
            dispatch(createUserRequestAction({
                id: '', role: userRole,
                login: userLogin, avatar: userAvatar, password: userPassword
            }))
        }
    }

    function roleOf(role: string) {
        // @ts-ignore
        return Role[role];
    }

    useEffect(() => {
        const extractedId = extractUserId(location.pathname);
        if (extractedId !== '' && extractedId.length === 36) {
            dispatch(fetchUserAndSetEdited(extractedId))
        } else {
            dispatch(setEditedUser(undefined))
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location.pathname]);

    useEffect(() => {
        setUserId(user !== undefined ? user.id : '');
        setUserLogin(user !== undefined ? user.login : '')
        setUserPassword(user !== undefined ? user.password : '')
        setUserAvatar(user !== undefined ? user.avatar : '')
        setUserRole(user !== undefined ? user.role : Role.USER)
    }, [user])

    function handleRoleSelection(value: string) {
        setUserRole(roleOf(value))
    }

    function handleBack() {
        dispatch(setEditedUser(undefined))
        history.goBack();
    }

    return (
        <div className={styles.container}>
            <Button label='Back' icon='arrow left' onClick={() => handleBack()} />
            <Form loading={isFetching}>
                <FormInput label='ID' disabled value={userId}/>
                <FormInput label='Login' value={userLogin} onChange={(_, data) => setUserLogin(data.value)}/>
                <FormInput label='Password' value={userPassword} onChange={(_, data) => setUserPassword(data.value)}/>
                <FormInput label='Avatar URL' value={userAvatar} onChange={(_, data) => setUserAvatar(data.value)}/>
                <img className={styles.image} src={userAvatar} alt=""/>
                <FormDropdown options={Object.keys(Role).map(r => ({text: r, value: roleOf(r)}))} value={userRole}
                              onChange={(_, data) => handleRoleSelection(data.value as any)}
                                label='Role'
                />
                <FormButton content='Save' onClick={() => handleSave()} loading={isEditing} color='yellow'/>
            </Form>
        </div>
    )
}

const mapStateToProps = (state: AppState) => ({
    user: state.users.editedUser,
    isEditing: state.async_edit_user.loading,
    isFetching: state.async_set_edited_user.loading,
})

export default connect(mapStateToProps)(UsersEditorPage);
