import {connect, useDispatch} from 'react-redux';
import AppState from '../../../state/state-type';
import {User} from '../../../state/model-types';
import React, {useEffect} from 'react';
import {UserTable} from '../../components/dumb/user-list-table/UserListTable';
import {useHistory} from 'react-router-dom';
import {deleteUserRequestAction, fetchUserAndSetEdited, fetchUsersRequestAction} from '../../../state/user-list/sagas';
import Spinner from '../../components/dumb/spinner/Spinner';
import {Button} from 'semantic-ui-react';
import styles from './styles.module.scss';

export interface UserListPageProps {
    users: Array<User>,
    isFetching: boolean,
    isDeleting: boolean,
    idDeleting: string | undefined
}

export function UsersListPage({users, isFetching, isDeleting, idDeleting}: UserListPageProps) {
    const history = useHistory();
    const dispatch = useDispatch();

    function editUser(id: string) {
        dispatch(fetchUserAndSetEdited(id))
        history.push('/admin/users/edit/' + id);
    }

    function deleteUser(id: string) {
        dispatch(deleteUserRequestAction(id));
    }

    useEffect(() => {
        dispatch(fetchUsersRequestAction())
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    function handleNewUser() {
        history.push('/admin/users/edit')
    }

    function handleChat() {
        history.push('/')
    }

    return (
        <>
            {isFetching ? <Spinner/> : (
                <div className={styles.container}>
                    <Button onClick={() => handleNewUser()} content='New User' color='green'/>
                    <Button onClick={() => handleChat()} content='Back to chat' color='blue'/>
                    <UserTable users={users} editUser={editUser} deleteUser={deleteUser}
                               isDeleting={isDeleting} idDeleting={idDeleting}
                    />
                </div>
            )}
        </>
    )
}

const mapStateToProps = (state: AppState) => ({
    users: state.users.users,
    isFetching: state.async_fetch_users.loading,
    isDeleting: state.async_delete_user.loading,
    idDeleting: state.async_delete_user.payload
})

// @ts-ignore
export default connect(mapStateToProps)(UsersListPage);
