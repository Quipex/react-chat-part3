interface BaseDto {
    id: string,
    createdAt: string,
    updateAt: string
}

export interface UserDto extends BaseDto {
    login: string,
    password: string,
    avatar: string,
    role: string
}

export interface MessageDto extends BaseDto {
    text: string,
    liked: boolean
    author: UserDto
}

export interface ChatDto extends BaseDto {
    name: string,
    users: number,
    messages: number
}
