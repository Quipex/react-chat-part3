import {all, call, put, takeEvery} from 'redux-saga/effects'
import axios from 'axios';
import {backendUrl} from '../../server-api/constants';
import {reduxHelper} from '../common/request-success-failure-helper';
import {MessageDto} from '../../server-api/types';
import {
    deleteMessage,
    editMessage,
    likeMessage,
    loadChat,
    sendMessage,
    setConfiguredMessage,
    setEditedMessage
} from './actions';
import {Message, User} from '../model-types';
import {userDtoToUser} from '../profile/sagas';

export const {actionTypes: fetchChatActions, reducer: fetchChatReducer} = reduxHelper('fetch_chat');

export function fetchChatRequestAction(userId: string) {
    return {type: fetchChatActions.request, payload: {userId}};
}


export const {actionTypes: sendMessageActions, reducer: sendMessageReducer} = reduxHelper('send_message');

export function sendMessageRequestAction(author: User, text: string, chatId: string) {
    return {type: sendMessageActions.request, payload: {authorId: author.id, text, chatId}}
}


export const {reducer: editMessageReducer, actionTypes: editMessageActions} = reduxHelper('edit_message');

export function editMessageRequestAction(id: string, text: string) {
    return {type: editMessageActions.request, payload: {id, text}}
}


export const {reducer: deleteMessageReducer, actionTypes: deleteMessageActions} = reduxHelper('delete_message');

export function deleteMessageRequestAction(messageId: string) {
    return {type: deleteMessageActions.request, payload: {messageId}}
}


export const {reducer: likeMessageReducer, actionTypes: likeMessageActions} = reduxHelper('like_message');

export function likeMessageRequestAction(messageId: string, userId: string) {
    return {type: likeMessageActions.request, payload: {messageId, userId}}
}


export function* chatSagas() {
    yield all([
        watchFetchChat(),
        watchSendMessage(),
        watchEditMessage(),
        watchDeleteMessage(),
        watchLikedMessage()
    ])
}

function* watchFetchChat() {
    yield takeEvery(fetchChatActions.request, tryFetchChat)
}

function* tryFetchChat(action: { type: string, payload: { userId: string } }) {
    try {
        const chatsResponse = yield call(axios.get, backendUrl + 'chats');
        const chat = chatsResponse.data[0];
        const messagesResponse = yield call(axios.get,
            backendUrl + 'chats/' + chat.id + '/messages?userId=' + action.payload.userId);
        chat.messages = messagesResponse.data;
        const usersResponse = yield call(axios.get, backendUrl + 'chats/' + chat.id + '/users');
        chat.users = usersResponse.data;
        yield put({type: fetchChatActions.success})
        yield put(loadChat(chat))
    } catch (e) {
        yield put({type: fetchChatActions.failure, error: e})
    }
}

function* watchSendMessage() {
    yield takeEvery(sendMessageActions.request, trySendMessage);
}

function* trySendMessage(action: { type: string, payload: Message }) {
    try {
        const sendMessageResponse = yield call(axios.post, backendUrl + 'messages/create', action.payload);
        const sentMessage = sendMessageResponse.data as MessageDto;
        yield put({type: sendMessageActions.success});
        yield put(sendMessage(messageDtoToMessage(sentMessage)));
    } catch (e) {
        yield put({type: fetchChatActions.failure, error: e})
    }
}

function* watchEditMessage() {
    yield takeEvery(editMessageActions.request, tryEditMessage);
}

function* tryEditMessage(action: { type: string, payload: Message }) {
    try {
        const editMessageResp = yield call(axios.post, backendUrl + 'messages/update', action.payload);
        const editedMessage = editMessageResp.data as MessageDto;
        yield put({type: editMessageActions.success});
        yield put(editMessage(messageDtoToMessage(editedMessage)));
        yield put(setEditedMessage(undefined))
        yield put(setConfiguredMessage(undefined))
    } catch (e) {
        yield put({type: editMessageActions.failure, error: e});
    }
}

function* watchDeleteMessage() {
    yield takeEvery(deleteMessageActions.request, tryDeleteMessage);
}

function* tryDeleteMessage(action: { type: string, payload: {messageId: string} }) {
    try {
        yield call(axios.delete, backendUrl + 'messages/' + action.payload.messageId);
        yield put({type: deleteMessageActions.success})
        yield put(deleteMessage(action.payload.messageId));
    } catch (e) {
        yield put({type: deleteMessageActions.failure, error: e});
    }
}

function* watchLikedMessage() {
    yield takeEvery(likeMessageActions.request, tryLikeMessage);
}

function* tryLikeMessage(action: { type: string, payload: { messageId: string, userId: string } }) {
    try {
        const likeResponse = yield call(axios.put, backendUrl + 'messages/like', action.payload);
        const isLiked = likeResponse.data as boolean;
        yield put({type: likeMessageActions.success});
        yield put(likeMessage(action.payload.messageId, isLiked));
    } catch (e) {
        yield put({type: likeMessageActions.failure, error: e});
    }
}

export function messageDtoToMessage(dto: MessageDto): Message {
    return {...dto, author: userDtoToUser(dto.author)}
}
