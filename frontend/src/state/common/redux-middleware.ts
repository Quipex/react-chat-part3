import {Dispatch} from 'react';
import {AnyAction} from 'redux';

export function middleware({dispatch}: {dispatch: Dispatch<AnyAction>}) {
    return (next: Function) => (action: any) => {
        if (typeof action === 'function') {
            return action(dispatch)
        }

        return next(action)
    }
}
