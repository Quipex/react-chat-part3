// @ts-ignore
import {NotificationManager} from 'react-notifications';

export interface RequestSuccessFailureState {
    data: any,
    loading: boolean,
    error: any,
    payload: any
}

export interface ReduxAction {
    type: string,
    data: any,
    loading: boolean,
    error: any,
    payload: any,
    success: any
}

export function reduxHelper(actionName: string) {
    const actionNameUpper = actionName.toUpperCase()
    const actionRequest = actionNameUpper + '_REQUEST'
    const actionSuccess = actionNameUpper + '_SUCCESS'
    const actionFailure = actionNameUpper + '_FAILURE'

    const initialState: RequestSuccessFailureState = {
        data: null,
        loading: false,
        error: null,
        payload: null,
    }

    const reducer = (state = initialState, action: ReduxAction) => {
        switch (action.type) {
            case actionRequest:
                return {
                    ...state,
                    loading: true,
                    error: null,
                    data: null,
                    payload: action.payload
                }
            case actionSuccess:
                if (action.success) {
                    NotificationManager.info(action.success)
                }
                return {
                    ...state,
                    loading: false,
                    data: action.data !== undefined ? action.data : null,
                    error: null
                }
            case actionFailure:
                NotificationManager.error(action.error.message);
                return {
                    ...state,
                    loading: false,
                    error: action.error
                }
            default:
                return state
        }
    }

    return {
        actionTypes: {
            request: actionRequest,
            success: actionSuccess,
            failure: actionFailure
        },
        reducer
    }
}
