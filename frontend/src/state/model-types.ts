interface BaseEntity {
    id?: string,
    createdAt?: string,
    updatedAt?: string
}

export interface Chat extends BaseEntity {
    id: string,
    name: string,
    users: Array<User>,
    messages: Array<Message>
}

export interface Message extends BaseEntity {
    text: string,
    author: User,
    liked?: boolean,
}

export interface User {
    id: string,
    login: string,
    avatar: string,
    password: string,
    role: Role
}

export enum Role {
    ADMIN='ADMIN',
    USER='USER'
}
