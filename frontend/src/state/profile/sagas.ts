import {all, call, put, takeEvery} from "redux-saga/effects";
import axios from 'axios';
import {reduxHelper} from '../common/request-success-failure-helper';
import {backendUrl} from '../../server-api/constants';
import {UserDto} from '../../server-api/types';
import {setCurrentUser} from './actions';
import {Role, User} from '../model-types';

const {actionTypes, reducer} = reduxHelper('login');

export const loginReducer = reducer;

export function loginRequestAction(login: string, password: string) {
    return {type: actionTypes.request, payload: {login, password}}
}

export function* profileSagas() {
    yield all([watchLoginRequests()])
}

function* watchLoginRequests() {
    yield takeEvery(actionTypes.request, requestLogin)
}

function* requestLogin(action: any) {
    try {
        const loginResponse = yield call(axios.post, backendUrl + "login", action.payload)
        const userDto = loginResponse.data as UserDto
        yield put({type: actionTypes.success})
        yield put(setCurrentUser(userDtoToUser(userDto)))
    } catch (e) {
        yield put({type: actionTypes.failure, error: e})
    }
}

export function userDtoToUser(dto: UserDto): User {
    return {...dto, role: Role[dto.role as keyof typeof Role]}
}
