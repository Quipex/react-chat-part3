import {chatSagas} from './chat/sagas';
import {all} from 'redux-saga/effects';
import {profileSagas} from './profile/sagas';
import {usersSagas} from './user-list/sagas';

export default function* rootSaga() {
    yield all([
        chatSagas(),
        profileSagas(),
        usersSagas()
    ])
}
