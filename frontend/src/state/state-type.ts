import {ChatState} from './chat/types';
import {ProfileState} from './profile/types';
import {RequestSuccessFailureState} from './common/request-success-failure-helper';
import {UsersState} from './user-list/types';

export default interface AppState {
    chat: ChatState,
    profile: ProfileState,
    users: UsersState,
    async_login: RequestSuccessFailureState,
    async_fetch_chat: RequestSuccessFailureState,
    async_send_message: RequestSuccessFailureState,
    async_edit_message: RequestSuccessFailureState,
    async_like_message: RequestSuccessFailureState,
    async_delete_message: RequestSuccessFailureState,
    async_fetch_users: RequestSuccessFailureState,
    async_create_user: RequestSuccessFailureState,
    async_edit_user: RequestSuccessFailureState,
    async_delete_user: RequestSuccessFailureState,
    async_set_edited_user: RequestSuccessFailureState
}
