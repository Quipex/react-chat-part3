import {User} from '../model-types';
import {SET_ALL_USERS, SET_EDITED_USER, UsersActionTypes} from './types';

export function setAllUsers(users: Array<User>): UsersActionTypes {
    return {
        type: SET_ALL_USERS,
        payload: users
    }
}

export function setEditedUser(user: User | undefined): UsersActionTypes {
    return {
        type: SET_EDITED_USER,
        payload: user
    }
}
