import {UsersActionTypes, UsersState} from './types';

export default (state: UsersState = {users: []}, action: UsersActionTypes) => {
    switch (action.type) {
        case 'USERS:SET_EDITED_USER':
            return {
                ...state,
                editedUser: action.payload
            }
        case 'USERS:SET_ALL':
            return {
                ...state,
                users: action.payload
            }
        default:
            return state;
    }
}
