import {reduxHelper} from '../common/request-success-failure-helper';
import {all, call, put, takeEvery, takeLeading} from 'redux-saga/effects';
import {User} from '../model-types';
import axios from 'axios';
import {backendUrl} from '../../server-api/constants';
import {UserDto} from '../../server-api/types';
import {setAllUsers, setEditedUser} from './actions';
import {userDtoToUser} from '../profile/sagas';

export const {actionTypes: fetchUsersActions, reducer: fetchUsersReducer} = reduxHelper('fetch_users');

export function fetchUsersRequestAction() {
    return {type: fetchUsersActions.request}
}


export const {actionTypes: createUserActions, reducer: createUserReducer} = reduxHelper('create_user');

export function createUserRequestAction(user: User) {
    return {type: createUserActions.request, payload: user}
}


export const {actionTypes: editUserActions, reducer: editUserReducer} = reduxHelper('edit_user');

export function editUserRequestAction(user: User) {
    return {type: editUserActions.request, payload: user}
}


export const {actionTypes: deleteUserActions, reducer: deleteUserReducer} = reduxHelper('delete_user');

export function deleteUserRequestAction(userId: string) {
    return {type: deleteUserActions.request, payload: userId}
}


export const {actionTypes: setEditedUserActions, reducer: setEditedUserReducer} = reduxHelper('set_edited_user');

export function fetchUserAndSetEdited(userId: string) {
    return {type: setEditedUserActions.request, payload: userId}
}

export function* usersSagas() {
    yield all([
        watchFetchUsers(),
        watchCreateUser(),
        watchEditUser(),
        watchDeleteUser(),
        watchSetEditedUser()
    ])
}

function* watchFetchUsers() {
    yield takeLeading(fetchUsersActions.request, tryFetchUsers);
}

function* tryFetchUsers() {
    try {
        const usersResp = yield call(axios.get, backendUrl + 'users')
        const users = usersResp.data as Array<UserDto>;
        yield put({type: fetchUsersActions.success});
        yield put(setAllUsers(users.map(u => userDtoToUser(u))))
    } catch (e) {
        yield put({type: fetchUsersActions.failure, error: e})
    }
}

function* watchCreateUser() {
    yield takeEvery(createUserActions.request, tryCreateUser);
}

function* tryCreateUser(action: {type: string, payload: User}) {
    try {
        yield call(axios.post, backendUrl + 'users/create', action.payload);
        yield put({type: createUserActions.success, success: 'User created successfully!'});
        yield put(fetchUsersRequestAction());
    } catch (e) {
        yield put({type: createUserActions.failure, error: e})
    }
}

function* watchEditUser() {
    yield takeEvery(editUserActions.request, tryEditUser);
}

function* tryEditUser(action: {type: string, payload: User}) {
    try {
        yield call(axios.post, backendUrl + 'users/update', action.payload);
        yield put({type: editUserActions.success, success: 'User edited successfully!'});
        yield put(fetchUsersRequestAction());
    } catch (e) {
        yield put({type: editUserActions.failure, error: e});
    }
}

function* watchDeleteUser() {
    yield takeEvery(deleteUserActions.request, tryDeleteUser);
}

function* tryDeleteUser(action: {type: string, payload: string}) {
    try {
        yield call(axios.delete, backendUrl + 'users/' + action.payload);
        yield put({type: deleteUserActions.success, success: 'User deleted successfully!'})
        yield put(fetchUsersRequestAction())
    } catch (e) {
        yield put({type: deleteUserActions.failure, error: e})
    }
}

function* watchSetEditedUser() {
    yield takeEvery(setEditedUserActions.request, tryFetchUserAndSetEdited)
}

function* tryFetchUserAndSetEdited(action: {type:string, payload: string}) {
    try {
        const fetchResp = yield call(axios.get, backendUrl + 'users/' + action.payload);
        const fetchedUser = fetchResp.data as UserDto;
        yield put({type: setEditedUserActions.success});
        yield put(setEditedUser(userDtoToUser(fetchedUser)));
    } catch (e) {
        yield put({type: setEditedUserActions.failure, error: e});
    }
}
