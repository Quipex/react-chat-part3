import {User} from '../model-types';

export const SET_ALL_USERS = 'USERS:SET_ALL';
export const SET_EDITED_USER = 'USERS:SET_EDITED_USER';

export interface UsersState {
    users: Array<User>,
    editedUser?: User
}

interface SetAllUsersAction {
    type: typeof SET_ALL_USERS,
    payload: Array<User>
}

interface SetEditedUserAction {
    type: typeof SET_EDITED_USER,
    payload: User | undefined
}

export type UsersActionTypes = SetAllUsersAction | SetEditedUserAction;
