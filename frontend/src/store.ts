import {applyMiddleware, combineReducers, createStore} from 'redux';
import chatReducer from './state/chat/reducers';
import profileReducer from './state/profile/reducers';
import usersReducer from './state/user-list/reducers';
import {composeWithDevTools} from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga'
import {createLogger} from 'redux-logger';
import rootSaga from './state/sagas';
import {
    deleteMessageReducer,
    editMessageReducer,
    fetchChatReducer,
    likeMessageReducer,
    sendMessageReducer,
} from './state/chat/sagas';
import {loginReducer} from './state/profile/sagas';
import {
    createUserReducer,
    deleteUserReducer,
    editUserReducer,
    fetchUsersReducer,
    setEditedUserReducer
} from './state/user-list/sagas';

const reducers = {
    chat: chatReducer,
    profile: profileReducer,
    users: usersReducer,
    // auth
    async_login: loginReducer,
    // chat
    async_fetch_chat: fetchChatReducer,
    async_send_message: sendMessageReducer,
    async_edit_message: editMessageReducer,
    async_delete_message: deleteMessageReducer,
    async_like_message: likeMessageReducer,
    // users
    async_fetch_users: fetchUsersReducer,
    async_create_user: createUserReducer,
    async_edit_user: editUserReducer,
    async_delete_user: deleteUserReducer,
    async_set_edited_user: setEditedUserReducer
};

const sagaMiddleware = createSagaMiddleware();

const middlewares = [
    sagaMiddleware
];

if (process.env.NODE_ENV === 'development') {
    const logger = createLogger()
    middlewares.push(logger as any)
}

const rootReducer = combineReducers({...reducers});

const composedEnhancers = composeWithDevTools(
    applyMiddleware(...middlewares)
);

const store = createStore(
    rootReducer,
    composedEnhancers
);

sagaMiddleware.run(rootSaga);

export default store;
